﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Play : MonoBehaviour {
    #if !UNITY_ANDROID
    // esto funciona para una reproducción desde el editor, lo que lo limita a videos .ogv. Se usa en cambio un plugin para Android que reproduce .mp4 sólo en el .apk final
	// // http://stackoverflow.com/a/35044140
    // public Text progressGUI;
    // public MeshRenderer targetRender = null;
    // public AudioSource targetAudio = null;
    // // public string URLString = "http://192.168.0.121:8080/Videos/SampleVideo.ogv";	// sirve cuando se ejecuta "http-server unity" en la línea de comandos
    // public string URLString = "http://192.168.0.121:3000/videos";
    // MovieTexture loadedTexture;

    // IEnumerator Start() {

    //     if(targetRender ==null) targetRender = GetComponent<MeshRenderer> ();
    //     if(targetAudio ==null) targetAudio = GetComponent<AudioSource> ();

    //     WWW www = new WWW (URLString);
    //     while (www.isDone == false) {
    //         if(progressGUI !=null) progressGUI.text = "Progresso do video: " + (int)(100.0f * www.progress) + "%";
    //         yield return 0;
    //     }

    //     loadedTexture = www.movie;
    //     while (loadedTexture.isReadyToPlay == false) {
    //         yield return 0;
    //     }
    //     targetRender.material.mainTexture = loadedTexture;
    //     targetAudio.clip = loadedTexture.audioClip;
    //     targetAudio.Play ();
    //     loadedTexture.Play ();
    // }
    #endif
}